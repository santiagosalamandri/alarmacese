#ifndef _LEDDRIVER_H
#define _LEDDRIVER_H

#include "events.h"

typedef enum{
	OFF,
	ON,
} status_t;

typedef struct ledObject_t* ledObjectPtr ;
ledObjectPtr createLedObject(int gpio);
status_t getLedStatus(ledObjectPtr led);

event_t* getLedOnEvent();
event_t* getLedOffEvent();

void ledDriverInit();


#endif // _LEDDRIVER_H

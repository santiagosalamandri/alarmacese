#include "msgProcessor.h"
#include "alarm.h"
#include "stdio.h"

static event_t NEW_MSG_T;



event_t* getnewMsgEvent() {
	return &NEW_MSG_T;
}



static int isAValidCommand(void* args){

	return 1;
}

static void processMsgHandler(void* args) {

	if(!isAValidCommand(args))return;

	commandFormat_t* newCommand=(commandFormat_t*) args;


	switch (newCommand->command) {
		case CHECK_STATE:
			//TODO Send alarm state--> getAlarmStatus()
			break;
		case CREATE_USER:
			//TODO send event to CRUD Module
			break;
		case READ_USERS:
			//TODO send event to CRUD Module
			break;
		case UPDATE_USER:
			//TODO send event to CRUD Module
			break;
		case DELETE_USER:
			//TODO send event to CRUD Module
			break;
		case TRIGGER_RELAY:
			//TODO send event to Relay Module
			break;
		case TRIGGER_ALARM:
			printf("enviando trigger event %d\n",(int)getPanicEvent()->ev);
			triggerEvent(getPanicEvent());
			break;
		case ACTIVATE_ALARM:
			triggerEvent(getArmEvent());
			break;
		case DEACTIVATE_ALARM:
			triggerEvent(getDisarmEvent());
			break;
		default:
			break;
	}
}

void msgProcessorInit() {
	NEW_MSG_T.ev = getLastEventType();
	addHandle(NEW_MSG_T.ev, processMsgHandler);
}

#ifndef _ALARM_H
#define _ALARM_H

#include "events.h"

typedef struct alarm_t* alarmPtr;

typedef enum{OFFLINE,
	DISARMED,
	ARMING,
	ARMED,
	DISARMING,
	ALERT} alarmStatus;

int initAlarm();

alarmStatus getAlarmStatus();


/****************EVENTOS********************** */
event_t*  getDisarmEvent();
event_t*  getArmEvent();
event_t*  getPanicEvent();


#endif // _ALARMA_H

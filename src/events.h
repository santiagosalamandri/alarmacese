#ifndef _EVENTS_H
#define _EVENTS_H

#define MAX_REGISTERED_HANDLES 20
#define MAX_REGISTERED_EVENTS 20

typedef enum{
	PANIC=1,
	OPEN_DOOR,
	CLOSE_DOOR,
	ARM,
	DISARM,
	LAST_EVENT
}eventType;

typedef void(*handler)(void* args);

typedef struct {
	eventType ev;
	handler handler;
	int isUsed;
}handle_t;

typedef struct {
	eventType ev;
	void* args;
}event_t;


eventType getLastEventType();
void setLastEventType(eventType event) ;



void triggerEvent(event_t* event);
void removeEvent(event_t* event);



event_t* getEventQueue();
int getUnprocessedEventQty();
void processEvents();


int addHandle(eventType ev,handler eventHandler);
int removeHandle(eventType ev,handler eventHandler);

handle_t* getRegisteredHandles();
int getRegisteredHandlesQty();

void dispatchHandlers(event_t ev) ;
void printEventQueue();

#endif // _EVENTS_H

#include "ledDriver.h"
#include <stdlib.h>
#include <stdio.h>


typedef struct ledObject_t{
	int gpioPin;
	void* args;
	status_t status;
}ledObject_t;


static event_t SET_LED_ON_T;
static event_t SET_LED_OFF_T;

ledObjectPtr createLedObject(int gpio){

	ledObjectPtr ledObject= malloc(sizeof ledObject);
	if(ledObject){
		ledObject->gpioPin=gpio;
		ledObject->status=OFF;
		return ledObject;
	}
	else return NULL;
}

event_t* getLedOnEvent(){
	return &SET_LED_ON_T;
}

event_t* getLedOffEvent() {
	return &SET_LED_OFF_T;
}

void ledOnHandler(void* args){
	ledObjectPtr led=(ledObjectPtr)args;
printf("led %d encendido\n",(int)led->gpioPin);
led->status=ON;

}
void ledOffHandler(void* args){
	ledObjectPtr led=(ledObjectPtr)args;
	printf("led %d apagado\n",(int)led->gpioPin);
	led->status=OFF;
}

void ledDriverInit(){
//TODO inicializar gpio

	SET_LED_ON_T.ev = getLastEventType();
	SET_LED_OFF_T.ev = getLastEventType();

addHandle(	SET_LED_ON_T.ev, ledOnHandler);
addHandle(	SET_LED_OFF_T.ev, ledOffHandler);
}

status_t getLedStatus(ledObjectPtr led){
	return led->status;
}

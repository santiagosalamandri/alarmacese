#ifndef _DOORSENSOR_H
#define _DOORSENSOR_H

#include "events.h"

typedef enum{
	CLOSE,
	OPEN
}sensorStatus;

typedef struct sensor_t* sensorPtr;

sensorPtr createSensor();
sensorStatus getSensorStatus(sensorPtr sensor);

void setSensorStatus(sensorPtr sensor,sensorStatus status);


void sensorInit();
event_t* getOpenDoorEvent();

#endif // _DOORSENSOR_H

#include "alarm.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

static void disarmHandler(void* args);
static void armHandler(void* args);
static void panicHandler(void* args);
static void timerHandler(void* args);
static void doorSensorHandler(void* args);

static event_t ARM_T;
static event_t DISARM_T;
static event_t PANIC_T;

typedef struct alarm_t {
	uint32_t id;
	alarmStatus status;
	const char* password;
	uint8_t isInitalized;
} alarm_t;

alarm_t alarmObject = { .status = DISARMED, .id = 123456,
		.password = "password", .isInitalized = 0 };

alarmPtr getAlarm() {
	if (!initAlarm()) {
		return NULL;
	}
	return &alarmObject;
}

alarmStatus getAlarmStatus() {
	return alarmObject.status;
}

static void setAlarmStatus(alarmStatus status) {

	alarmObject.status = status;
}

int initAlarm() {
	if (!alarmObject.isInitalized) {
		/*Inicializo eventos propios **/
		ARM_T.ev = getLastEventType();
		DISARM_T.ev = getLastEventType();
		PANIC_T.ev = getLastEventType();

		//TODO agregar timer event

		/*Agrego handles*/
		addHandle(ARM_T.ev, armHandler);
		addHandle(DISARM_T.ev, disarmHandler);
		addHandle(PANIC_T.ev, panicHandler);

		alarmObject.isInitalized = 1;
		//TODO: read configuration from file
		return 1;

	} else
		return 0;
}

static void disarmHandler(void* args) {
	setAlarmStatus(DISARMED);
	//TODO enviar evento con mensaje de cambio de estado
}

static void armHandler(void* args) {
	if (getAlarmStatus() != ALERT) {
		setAlarmStatus(ARMED);
		//TODO enviar evento con mensaje de cambio de estado
	}
}

static void panicHandler(void* args) {
	setAlarmStatus(ALERT);
//TODO enviar evento con mensaje de cambio de estado
}
static void timerHandler(void* args) {
	if (getAlarmStatus() == DISARMING) {
		setAlarmStatus(ALERT);
		//TODO enviar evento con mensaje de cambio de estado
	}
}

static void doorSensorHandler(void* args) {
	if (getAlarmStatus() != ARMED) {
		setAlarmStatus(DISARMING);
		//TODO enviar evento con mensaje de cambio de estado
	}
}

event_t* getDisarmEvent() {
	return &DISARM_T;
}
event_t* getArmEvent() {
	return &ARM_T;
}
event_t* getPanicEvent() {
	return &PANIC_T;
}

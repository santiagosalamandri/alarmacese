#ifndef _MSGPROCESSOR_H
#define _MSGPROCESSOR_H

#include "events.h"

typedef enum{
	CHECK_STATE,
	CREATE_USER,
	READ_USERS,
	UPDATE_USER,
	DELETE_USER,
	TRIGGER_RELAY,
	TRIGGER_ALARM,
	ACTIVATE_ALARM,
	DEACTIVATE_ALARM,
} command;

typedef struct {
	command command;
	void* args;
}commandFormat_t;

event_t* getnewMsgEvent();
void msgProcessorInit();
#endif // _MSGPROCESSOR_H

#include "doorSensor.h"
#include <stdio.h>
#include <stdlib.h>

static event_t OPEN_DOOR_T;

typedef struct sensor_t{
	sensorStatus status;
}sensor_t;

static void sensorEventsInit(){
	OPEN_DOOR_T.ev= getLastEventType();
}

sensorPtr createSensor(){

	sensorPtr doorSensor=malloc(sizeof doorSensor);

	if(doorSensor){
		doorSensor->status=CLOSE;
		sensorEventsInit();
		return doorSensor;
	}
	return NULL;
}


sensorStatus getSensorStatus(sensorPtr sensor){

	return sensor->status;
}


void setSensorStatus(sensorPtr sensor,sensorStatus status){

	sensor->status=status;
	if(status==OPEN){
		triggerEvent(getOpenDoorEvent());
		//setAlarmStatus(ALERT);
		;
	}
}


event_t* getOpenDoorEvent(){
	return &OPEN_DOOR_T;
}

void sensorInit(){
	if(OPEN_DOOR_T.ev!=0)sensorEventsInit();
}

#include "events.h"
#include <stdio.h>
#include <stddef.h>

handle_t registeredHandles[MAX_REGISTERED_HANDLES] = { 0 };
event_t eventQueue[MAX_REGISTERED_EVENTS] = { 0 };
int registeredHandlesQty = 0;

eventType lastEvent = LAST_EVENT;

eventType getLastEventType() {
	return lastEvent++;
}

void setLastEventType(eventType event) {
	lastEvent = event;
}

int addHandle(eventType ev,handler eventHandler) {
	int i;
	for (i = 0; i < MAX_REGISTERED_HANDLES; ++i) {
		if (registeredHandles[i].isUsed == 0) {
			registeredHandles[i].ev = ev;
			registeredHandles[i].handler = eventHandler;
			registeredHandles[i].isUsed = 1;
			registeredHandlesQty++;
			//printf("handle de evento %d agregado\n", ev);
			return 1;
		}
	}
	return 0;
}

void removeEvent(event_t* event){

	int i;
	for (i = 0; i < MAX_REGISTERED_EVENTS; ++i) {
		if(eventQueue[i].ev == event->ev){
			eventQueue[i].ev=0;
			//printf("evento borrado\n");
		}
	}

}

int removeHandle(eventType ev,handler eventHandler) {
	int i;
	for (i = 0; i < MAX_REGISTERED_HANDLES; ++i) {
		if (registeredHandles[i].ev == ev && registeredHandles[i].handler == eventHandler) {
			registeredHandles[i].ev = 0;
			registeredHandles[i].handler = NULL;
			registeredHandles[i].isUsed = 0;
			registeredHandlesQty--;
			//printf("handle removido\n");
			return 1;
		}
	}
	return 0;
}

event_t* getEventQueue() {
	return eventQueue;
}

void dispatchHandlers(event_t ev) {
	int i;
	for (i = 0; i < registeredHandlesQty; ++i) {
		if (registeredHandles[i].ev == ev.ev) {
			registeredHandles[i].handler(ev.args);
		}
	}
}

void processEvents() {
	event_t* events = getEventQueue();
	int i;
	for (i = 0; i < MAX_REGISTERED_EVENTS; ++i) {
		if (eventQueue[i].ev != 0) {
			dispatchHandlers(eventQueue[i]);
		}

		events[i].ev = 0; //*Termine de procesar envento, entonces lo borro
		events[i].args = NULL;
	}
}

void triggerEvent(event_t* event){
	int i;
	for (i = 0; i < MAX_REGISTERED_EVENTS; ++i) {
		if (eventQueue[i].ev == 0) {
			eventQueue[i].ev = event->ev;
			eventQueue[i].args = event->args;
			//printf("nuevo evento %d\n",event->ev);
			return;
		}
	}
}

void printEventQueue() {
	int i;
	int events=0;
	for (i = 0; i < MAX_REGISTERED_EVENTS; ++i) {
		if (eventQueue[i].ev != 0){
			printf("event[%d]: %d\n", i, eventQueue[i].ev);
			events=1;
		}

	}
	if(!events)printf("No events.\n");
}


handle_t* getRegisteredHandles(){
	return registeredHandles;

}

int getRegisteredHandlesQty(){
	return registeredHandlesQty;
}

int getUnprocessedEventQty(){
int i;
int qty=0;
for (i = 0; i < MAX_REGISTERED_EVENTS; ++i) {
	if(eventQueue[i].ev != 0)qty++;
}
return qty;
}

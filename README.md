# Trabajo Práctico 3 de la Materia Testing en SE: "TDD"

## Objetivos

* Seleccionar una biblioteca (o componente) del software a desarrollar en el TP final de la carrera de especialización / maestría y desarrollar el código siguiendo la metodología TDD (Test driven development).
* Se debe mantener bajo control de versión el código generado y se debe ir realizando
commits del mismo luego de escribir cada test unitario.


#### Entregar:
* Repositorio con los casos de prueba (preferentemente en bitbucket o github).


## Desarrollo:

Se va a desarrollar el modulo que controle el estado de una Alarma.

#### Requerimientos:

* La alarma debe poder mantener un estado.
* La alarma debe poder cambiar de estado.
* La alarma debe emitir una alerta,cuando su estado sea "ALARM".
* La alarma debe poder recibir senales de un sensor. 

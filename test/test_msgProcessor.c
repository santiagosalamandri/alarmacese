#include "unity.h"
#include "msgProcessor.h"

//IMPORTANT Agregar todas las librerias usadas por el modulo a probar
#include "events.h"
#include "alarm.h"
#include "doorSensor.h"

event_t colaEventos[] = { { PANIC, NULL }, { DISARM, NULL }, { PANIC, NULL }, {
		CLOSE_DOOR, NULL} };

void setUp(void)
{
	initAlarm();
	sensorInit();
	msgProcessorInit();

}

void tearDown(void)
{
}

void test_msgProcessor_processNewMsgAndSendEvent(void)
{
	commandFormat_t args={
			.command=TRIGGER_ALARM,
			.args=NULL};

	event_t* newEvent=getnewMsgEvent();
	newEvent->args= (void*) &args;

	triggerEvent(newEvent);

	processEvents();
	TEST_ASSERT_EQUAL(ALERT, getAlarmStatus());

}

#include "unity.h"
#include "ledDriver.h"
#include "events.h"

void setUp(void) {
	ledDriverInit();
}

void tearDown(void) {
}

void test_ledDriver_createLedObjectIsNotNULL(void) {
	ledObjectPtr ledObject = createLedObject(25);

	TEST_ASSERT_EQUAL_INT(getLedStatus(ledObject), 0);
}

void test_ledDriver_processLedOnEvent(void) {
	ledObjectPtr ledObject = createLedObject(10);

	event_t* onEvent = getLedOnEvent();
	onEvent->args = (void*) ledObject;

	triggerEvent(onEvent);
	processEvents();

	TEST_ASSERT_EQUAL_INT(getLedStatus(ledObject), ON);
}

void test_ledDriver_processLedOffEvent(void) {

	ledObjectPtr ledObject = createLedObject(10);

	event_t* onEvent = getLedOnEvent();
	onEvent->args = (void*) ledObject;

	triggerEvent(onEvent);

	event_t* offEvent = getLedOffEvent();
	offEvent->args = (void*) ledObject;
	triggerEvent(offEvent);

	processEvents();

	TEST_ASSERT_EQUAL_INT(getLedStatus(ledObject), OFF);
}

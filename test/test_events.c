#include "unity.h"
#include "events.h"

typedef struct {
	int a;
	char* b;
} params;

void panic(void* args) {
	//printf("hola Panic\n");
}
void closeDoor(void* args) {

	params* param = (params*) args;

	//printf("hola closeDoor: Dato pasado desde el evento: %d!!\n", param->a);
}
void handlerDePrueba(void* args) {

	params* param = (params*) args;

	//printf("hola Handler de prueba: Dato pasado desde el evento: %s!!\n",param->b);
}

params parametroDePrueba = { .a = 10, .b = "santi" };

event_t eventoPrueba = { 0 };

event_t colaEventos[] = { { PANIC, NULL }, { DISARM, NULL }, { PANIC, NULL }, {
		CLOSE_DOOR, (void*) &parametroDePrueba } };

eventType eventos[] = { PANIC, CLOSE_DOOR };
handler handlers[] = { panic, closeDoor };
int canthandlers = sizeof handlers / sizeof handlers[0];
int cantEventos = sizeof colaEventos / sizeof colaEventos[0];

void setUp(void) {

	int i;
	for (i = 0; i < canthandlers; ++i) {
		addHandle(eventos[i], handlers[i]);
	}

	for (i = 0; i < cantEventos; ++i) {
		triggerEvent(&colaEventos[i]);
	}

}

void tearDown(void) {
	int i;
	for (i = 0; i < canthandlers; ++i) {
		removeHandle(eventos[i], handlers[i]);
	}

	for (i = 0; i < cantEventos; ++i) {
		removeEvent(&colaEventos[i]);
	}

}

void test_events_addEventsToQueue(void) {

	event_t* eventQueue = getEventQueue();
	int i;
	for (i = 0; i < cantEventos; ++i) {
		TEST_ASSERT_EQUAL_INT(colaEventos[i].ev, eventQueue[i].ev);
	}

}

void test_events_addHandlesToBeProcessed(void) {

	int handlesQty = getRegisteredHandlesQty();

	TEST_ASSERT_EQUAL_INT(canthandlers, handlesQty);

}

void test_events_processEvent(void) {

	//printf("eventos antes de procesar:\n");
	//printEventQueue();
	TEST_ASSERT_EQUAL_INT(cantEventos, getUnprocessedEventQty());
	processEvents();
	TEST_ASSERT_EQUAL_INT(0, getUnprocessedEventQty());

	//printf("eventos despues de procesar:\n");
	//printEventQueue();

}

void test_events_processNewEventType(void) {

	//nuevo tipo de evento
	eventoPrueba.ev = getLastEventType();

	//agregar nuevo handle
	addHandle(eventoPrueba.ev, handlerDePrueba);

	eventoPrueba.args = (void*) &parametroDePrueba;
	triggerEvent(&eventoPrueba);

	//procesar evento
	processEvents();
	TEST_ASSERT_EQUAL_INT(0, getUnprocessedEventQty());

}

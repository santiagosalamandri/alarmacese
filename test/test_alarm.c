#include "unity.h"

#include "alarm.h"
#include "events.h"

//#include "doorSensor.h"

alarmPtr alarm;

void setUp(void) {
	initAlarm();
}

void tearDown(void) {
}



void test_alarm_getAlarmStatus(void) {
	triggerEvent(getDisarmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(DISARMED, getAlarmStatus());
}

void test_alarm_setAlarmStatus(void) {
	triggerEvent(getArmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(ARMED, getAlarmStatus());
}

void test_alarm_changeToAlertWhenPanicEventIsTriggered(void) {

	triggerEvent(getPanicEvent());
	processEvents();
	TEST_ASSERT_EQUAL(ALERT, getAlarmStatus());

}

void test_alarm_changeToArmedWhenArmedEventIsTriggeredAndStatusIsNotAlert(void) {

	triggerEvent(getDisarmEvent());
	triggerEvent(getArmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(ARMED, getAlarmStatus());

	triggerEvent(getDisarmEvent());
	triggerEvent(getArmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(ARMED, getAlarmStatus());

	triggerEvent(getDisarmEvent());
	triggerEvent(getArmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(ARMED, getAlarmStatus());

	triggerEvent(getDisarmEvent());
	triggerEvent(getArmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(ARMED, getAlarmStatus());

}

void test_alarm_NoChangeToArmedWhenArmedEventIsTriggeredAndStatusIsAlert(void) {

	triggerEvent(getPanicEvent());
	triggerEvent(getArmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(ALERT, getAlarmStatus());
}

void test_alarm_changeToDisarmedWhenDisarmedEventIsTriggered(void) {

	triggerEvent(getArmEvent());

	triggerEvent(getDisarmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(DISARMED, getAlarmStatus());

	triggerEvent(getPanicEvent());
	triggerEvent(getDisarmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(DISARMED, getAlarmStatus());

	//TODO trigger timer event
	triggerEvent(getDisarmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(DISARMED, getAlarmStatus());

	triggerEvent(getDisarmEvent());
	triggerEvent(getDisarmEvent());
	processEvents();
	TEST_ASSERT_EQUAL(DISARMED, getAlarmStatus());
}

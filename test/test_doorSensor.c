#include "unity.h"
#include "doorSensor.h"
#include "events.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_doorSensor_getPtrToSensorWhenCreateSensor(void)
{
	TEST_ASSERT_NOT_NULL(createSensor());

}

void test_alarm_getSensorStatus(void)
{
	sensorPtr sensor=createSensor();
	TEST_ASSERT_EQUAL(CLOSE,getSensorStatus(sensor));
}


void test_alarm_setSensorStatus(void)
{
	//printEventQueue();

	sensorPtr sensor=createSensor();
	setSensorStatus(sensor,OPEN);

	sensorStatus status=getSensorStatus(sensor);
	//printEventQueue();
	TEST_ASSERT_EQUAL(OPEN,status);
}


